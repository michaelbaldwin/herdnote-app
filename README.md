# herdnote-app

Try it out! [www.herdnote.co](https://www.herdnote.co/)

![Herdnote Demo](herdnote.gif)

## Idea
Build a simple clone of Flocknote using server-side tech

## Current Setup
- Serverless Netlify Function
- Jamstack setup on Netlify
- Static site build with Webpack
- Continuous deployment from git with Netlify
- DNS and SSL with Netlify
- Domain name with Namecheap
- Send email with Twilio Sendgrid

## Project Learning
This project took a lot of iterations as I continued to learn new things and simplify. You can checkout each iteration of work in its git branch.

1. AWS EC2: [aws-ec2](https://bitbucket.org/michaelbaldwin/herdnote-app/branch/aws-ec2)
2. AWS Lambda: [aws-lambda](https://bitbucket.org/michaelbaldwin/herdnote-app/src/aws-lambda/)
3. Netlify [netlify](https://bitbucket.org/michaelbaldwin/herdnote-app/src/netlify)
4. Netlify with React [react](https://bitbucket.org/michaelbaldwin/herdnote-app/src/react)

## Future Improvements
- Testing
- Form validation
- Better error messages
- Support sending to multiple email addresses
- Support sending text messages
