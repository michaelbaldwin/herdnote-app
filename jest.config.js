module.exports = {
  // The root directory that Jest should scan for tests and modules within
  rootDir: "src/",

  // mock static assets imported using webpack loaders
  // source: https://jestjs.io/docs/webpack#handling-static-assets
  moduleNameMapper: {
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "./__mocks__/fileMock.js",
    "\\.(css|less)$": "./__mocks__/styleMock.js",
  },

  setupFiles: ["./__mocks__/envVarMock.js"],
};
