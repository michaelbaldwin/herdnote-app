import React from 'react';

import { e } from '../react-util';
import './testimonials.css';
import PaulImg from './paul.jpg';
import PeterImg from './peter.jpg';
import JohnImg from './john.jpg';

function Testimonials() {
  return e('div', { className: 'testimonialsContainer' },
    e('h2', { key: 'h2' }, 'Testimonials'),
    e('ul', { key: 'ul' }, [
      e('li', { key: 'paul' }, e(Paul, { key: 'paulitem' })),
      e('li', { key: 'peter' }, e(Peter, { key: 'peteritem' })),
      e('li', { key: 'james' }, e(John, { key: 'johnitem' })),
    ]),
  );
}

function Paul() {
  return e('div', { className: 'testimonialItem' }, [
    e('img', { key: 'imgpaul', className: 'testimonialImage', src: PaulImg }),
    e('h3', { key: 'namePaul', className: 'testimonialName' }, 'Paul'),
    e('p', { key: 'textPaul', className: 'testimonialText' },
      '"I used to write old-fashioned letters to churches. Herdnote is so easy to use that I sometimes dash off an email newsletter while waiting in line at airport security. #Winning"'),
  ]);
}

function Peter() {
  return e('div', { className: 'testimonialItem' }, [
    e('img', { key: 'imagePeter', className: 'testimonialImage', src: PeterImg }),
    e('h3', { key: 'namePeter', className: 'testimonialName' }, 'Peter'),
    e('p', { key: 'textPeter', className: 'testimonialText' },
      '"At first I thought Herdnote was just a new fad, but Paul convinced me to try it out at the Council of Jerusalem. Now my papal addresses reach the faithful with the click of a button."'),
  ]);
}

function John() {
  return e('div', { className: 'testimonialItem' }, [
    e('img', { key: 'imageJohn', className: 'testimonialImage', src: JohnImg }),
    e('h3', { key: 'nameJohn', className: 'testimonialName' }, 'John'),
    e('p', { key: 'textJohn', className: 'testimonialText' },
      '"What\'s not to love about Herdnote? Not only is the app great, but the customer support team is even better! From what I hear, GIFs and Emojis are coming to Herdnote soon :)"'),
  ]);
}

export { Testimonials };
