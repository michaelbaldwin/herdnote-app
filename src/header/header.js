import React from 'react';

import { e } from '../react-util';
import SheepImg from './sheep.png';
import './header.css';

function Header() {
  return e('div', { className: 'headerContainer' },
    e('div', { key: 'herdnoteContainer', className: 'herdnoteContainer' }, [
      e('img', { key: 'imageSheep', className: 'sheepImg', src: SheepImg }),
      e('h1', { key: 'h1' }, 'Herdnote'),
    ]),
    e('p', { key: 'p' }, 'Having trouble reaching your flock? Herdnote can help!')
  );
}

export { Header };
