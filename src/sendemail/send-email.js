import React, { useState } from 'react';
import DOMPurify from 'dompurify';

import { e } from '../react-util';
import './send-email.css';

function SendEmail() {
  const [to, setTo] = useState('');
  const [subject, setSubject] = useState('');
  const [message, setMessage] = useState('');
  const [showSpinner, setShowSpinner] = useState(false);
  const [showSendResult, setShowSendResult] = useState(false);
  const [sendResultIsError, setSendResultIsError] = useState(false);
  const [sendResultMessage, setSendResultMessage] = useState('');

  return e('div', { className: 'sendEmailContainer' },
      e('h2', { key: 'h2' }, 'Try Herdnote out!'),
      e('div', { key: 'div', className: 'sendEmailFormContainer' }, [
        e(To, {
          key: 'to',
          value: to,
          onChange: (event) => setTo(event.target.value),
        }),
        e(Subject, {
          key: 'subject',
          value: subject,
          onChange: (event) => setSubject(event.target.value),
        }),
        e(Message, {
          key: 'message',
          value: message,
          onChange: (event) => setMessage(event.target.value),
        }),
        e(SendButton, {
          key: 'sendbutton',
          showSpinner: showSpinner,
          showSendResult: showSendResult,
          sendResultIsError: sendResultIsError,
          sendResultMessage: sendResultMessage,
          onClick: () => {
            setShowSendResult(false);
            setShowSpinner(true);
            onClickSend(to, subject, message, setShowSpinner,
                      setShowSendResult, setSendResultIsError, setSendResultMessage);
          }
        }),
      ]),
  );
}

function To({ value, onChange }) {
  return e('div', { className: 'sendEmailItem sendEmailToContainer' },
    e('label', { key: 'label' }, 'To: '),
    e('input', {
      key: 'inputTo',
      className: 'sendEmailTo',
      type: 'text',
      value: value,
      onChange: onChange,
    }),
  );
}

function Subject({ value, onChange }) {
  return e('div', { className: 'sendEmailItem sendEmailSubjectContainer' },
    e('label', { key: 'label' }, 'Subject: '),
    e('input', {
      key: 'inputSubject',
      className: 'sendEmailSubject',
      type: 'text',
      value: value,
      onChange: onChange,
    }),
  );
}

function Message({ value, onChange }) {
  return e('div', { className: 'sendEmailItem sendEmailMessageContainer' },
    e('label', { key: 'label' }, 'Message: '),
    e('textarea', {
      key: 'textareaMessage',
      className: 'sendEmailMessage',
      value: value,
      onChange: onChange,
    }),
  );
}

function SendButton({ onClick, showSpinner, showSendResult, sendResultIsError, sendResultMessage }) {
  return e('div', { className: 'sendEmailItem sendEmailButtonContainer' },
    e(Spinner, { key: 'spinner', showSpinner: showSpinner }),
    e(SendResult, { key: 'result', showSendResult: showSendResult, isError: sendResultIsError, message: sendResultMessage }),
    e('button', { key: 'button', className: 'sendEmailButton', onClick: onClick }, 'Send Email'),
  );
}

function Spinner({ showSpinner }) {
  if (!showSpinner) {
    return null;
  }
  return e('div', { className: 'spinnerContainer' },
    e('div', { key: 'div', className: 'spinner' }),
  );
}

function SendResult({ showSendResult, isError, message }) {
  if (!showSendResult) {
    return null;
  }
  if (isError) {
    return e('p', { className: 'sendResultError' }, `${message}`);
  }
  return e('p', { className: 'sendResultSuccess' }, `${message}`);
}

function onClickSend(toInput, subjectInput, messageInput, setShowSpinner,
                     setShowSendResult, setSendResultIsError, setSendResultMessage) {

  console.log(`User input. To: ${toInput}, Subject: ${subjectInput}, Message: ${messageInput}`);

  const cleanedToInput = DOMPurify.sanitize(toInput);
  const cleanedSubjectInput = DOMPurify.sanitize(subjectInput);
  const cleanedMessageInput = DOMPurify.sanitize(messageInput);
  console.log(`Send message to email address: ${cleanedToInput}, with subject: ${cleanedSubjectInput}, and message: ${cleanedMessageInput}`);

  const to = [cleanedToInput];
  const subject = cleanedSubjectInput
  const text = cleanedMessageInput;
  const html = cleanedMessageInput;

  netlifyFunctionSendEmail(to, subject, text, html)
    .then((response) => {
       console.log(`Success! Email sent: ${response}`);
       setShowSpinner(false);
       setShowSendResult(true);
       setSendResultIsError(false);
       setSendResultMessage('Email sent!');
    })
    .catch((error) => {
       console.error(`Error! Email not sent: ${error}`);
       setShowSpinner(false);
       setShowSendResult(true);
       setSendResultIsError(true);
       setSendResultMessage("Oops! Email didn't send.");
    });
}

async function netlifyFunctionSendEmail(toValue, subjectValue, textValue, htmlValue) {
  const url = '/.netlify/functions/sendemail';
  const body = {
    to: toValue,
    subject: subjectValue,
    text: textValue,
    html: htmlValue,
  };

  let response = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  }); 
  
  if (!response.ok) {
    throw new Error(`HTTP error! status: ${response.status}`);
  }
  
  return await response.json();
}

// mock a network call for development
async function mockNetlifyFunction(toValue) {
  return new Promise((resolve, reject) => {
    if (toValue[0].length > 2) {
      setTimeout(() => resolve('mock success'), 2000);
    } else {
      setTimeout(() => reject('mock error'), 2000);
    }
  });
}

export { SendEmail };
