import React, { useState } from 'react';

import { e } from '../react-util';
import { Header } from '../header';
import { SendEmail } from '../sendemail';
import { Testimonials } from '../testimonials';
import { Footer } from '../footer';
import './app.css';

class App extends React.Component {
  render() {
    return e('div', { className: 'appContainer' },
      e(Header, { key: 'header' }),
      e(SendEmail, { key: 'sendemail' }),
      e(Testimonials, { key: 'testimonials' }),
      e(Footer, { key: 'footer' }),
    );
  }
}

export { App };
