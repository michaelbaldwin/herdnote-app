const sgMail = require('@sendgrid/mail');

console.log('set sendgrid api key');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const FROM_EMAIL_ADDRESS = 'hello@herdnote.co';

const defaultMsg = {
  to: ['mtbaldwin10@gmail.com'],
  from: FROM_EMAIL_ADDRESS,
  subject: 'Herdnote',
  text: 'Have you heard?',
  html: '<strong>Easy as sending a note</strong>',
};

async function sendEmail(msg = defaultMsg) {
  try {
    console.log('Try to send email with message.', msg);
    await sgMail.send(msg);
    console.log('Success! Email sent.');

    return {
      emailSent: true,
      statusCode: 200,
      body: {
        message: msg,
      },
    };
  } catch (error) {
    console.error('Error! Email not sent.', error);
 
    if (error.response) {
      console.error('Error body.', error.response.body)
    }
    
    return {
      emailSent: false,
      statusCode: error.code || 400,
      body: {
        error: error,
        message: msg,
      },
    };
  }
}

module.exports = {
  sendEmail,
};
