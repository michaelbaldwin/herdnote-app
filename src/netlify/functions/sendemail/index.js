var SendGrid = require('./sendgrid');

const FROM_EMAIL_ADDRESS = 'hello@herdnote.co';

exports.handler = async function(event, context) {
  const parseResult = parseRequestBody(event);
  if (!parseResult.parsed) {
    return {
      statusCode: 400,
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(parseResult.body),
    };
  }
  const body = parseResult.body || {};
  const to = body.to || [];
  const subject = body.subject || '';
  const text = body.text || '';
  const html = body.html || '';

  const msg = {
    to: to,
    from: FROM_EMAIL_ADDRESS,
    subject: subject,
    text: text,
    html: html, 
  }
  const sendEmailResponse = await SendGrid.sendEmail(msg);
  if (!sendEmailResponse.emailSent) {
    return {
      statusCode: sendEmailResponse.statusCode,
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(sendEmailResponse.body),
    };
  }

  return {
    statusCode: sendEmailResponse.statusCode,
    headers: {
      'Content-Type': 'application/json'
    },      
    body: JSON.stringify(sendEmailResponse.body),
  };
}

function parseRequestBody(event) {
  try {
    const obj = JSON.parse(event.body);
    return { parsed: true, body: obj };
  } catch (error) {
    console.log('Error! Did not parse request body.', error);
    return { parsed: false, body: error };
  }
}
