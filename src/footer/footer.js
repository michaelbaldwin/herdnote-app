import React from 'react';

import { e } from '../react-util';
import './footer.css';

function Footer() {
  return e('div', { className: 'footerContainer' },
    e('p', { key: 'p' }, 'Crafted with ❤️  in Michigan'),
  );
}

export { Footer };
