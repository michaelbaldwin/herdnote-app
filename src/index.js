import React from 'react';
import ReactDOM from 'react-dom';

import { App } from './app';

if (process.env.NODE_ENV !== 'production') {
  console.info('development mode');
}

function render() {
  const appDiv = document.createElement('div');
  document.body.appendChild(appDiv);

  ReactDOM.render(
    React.createElement(App, {}, null),
    appDiv
  );
}
render();
