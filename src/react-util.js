import React from 'react';

// https://reactjs.org/docs/react-without-jsx.html
// React.createElement(component, props, ...children)

function e(element, props = null, ...children) {
  // input tag can't have children
  if (children.length == 0) {
    return React.createElement(element, props);
  }
  return React.createElement(element, props, children); 
}

export { e };
